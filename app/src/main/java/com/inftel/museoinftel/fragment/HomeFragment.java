package com.inftel.museoinftel.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageSwitcher;

import com.inftel.museoinftel.R;
import com.inftel.museoinftel.service.RequestEventsTask;
import com.inftel.museoinftel.utility.Conexion;
import com.inftel.museoinftel.utility.DropboxConnection;

public class HomeFragment extends Fragment {

    DropboxConnection dc;
    private ImageSwitcher imageSwitcher;
    Button btnNext;

    int imageIds[]={R.drawable.cuadro1,R.drawable.cuadro2,R.drawable.cuadro3,R.drawable.cuadro4,R.drawable.cuadro5,R.drawable.cuadro6,};
    int messageCount=imageIds.length;
    int currentIndex=-1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dc = new DropboxConnection(getActivity());
        dc.connect();
    }

    @Override
    public void onResume(){
        super.onResume();
        dc.resume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);
        new RequestEventsTask(v, getActivity(), dc.getDropboxApi()).execute(Conexion.MUSEO_SERVER + Conexion.EVENT_RESOURCE);
        /*
        btnNext=(Button)v.findViewById(R.id.buttonNext);
        imageSwitcher = (ImageSwitcher) v.findViewById(R.id.imageSwitcher);

        imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {

                ImageView imageView = new ImageView(getActivity().getApplicationContext());
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, Gallery.LayoutParams.FILL_PARENT));
                return imageView;
            }
        });

        Animation in = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(getActivity(),android.R.anim.slide_out_right);

        imageSwitcher.setInAnimation(in);
        imageSwitcher.setOutAnimation(out);

        btnNext.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                currentIndex++;
                if(currentIndex==messageCount)
                    currentIndex=0;
                imageSwitcher.setImageResource(imageIds[currentIndex]);
            }
        });
        */

        return v;
    }

}
